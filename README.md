# AgriXHorizon
AgriXHorizon is a team of two, Clarence Paintsil and Siti Nabila Mohd Radzi, working on sharing information of how climate change has affected the agriculture industry in the world. In this project, named as AgriXHorizon_CloudSeeding, we focus on understanding the technology of cloud seeding for weather modification, as one of the method to combat drought by improving the precipitation and promoting a suitable ecosystem for crop production. 

## Description
We are using Python for data analysis, simulation and visualization where we will simulate data of the precipitation to recognize the pattern and use the data to locate area that is needed for cloud seeding and see how it affects the quality of soil, water supply and the crop production, as a whole. How? 

## How?
We will visualize the cloud environment, utilize data from previous weather trends to analyze if patterns occur and using a Weather API to store information into 1 concise structure.

## Authors and acknowledgment
We would like to thank Kreative Horizon by Horizon Builders Club for giving us this opportunity to challenge ourselves to solve problems through 24-hour HackIowaStateV2. This is definitely the best event where we get to learn new things, and using the tools and knowledge to solve problems around us.


## Project status
The project is on the planning process, where we plan on utilizing Python as a tool for data analysis, simulation and visualisation in sharing the information on how cloud seeding improves weather and crop production.
